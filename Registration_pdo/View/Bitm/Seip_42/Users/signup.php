<?php
include_once ("../../../../include/header.php");
//include ("include/header.php");
?>
<!--Registration form - START -->
<div class="container">
    <div class="row">
        <form role="form" method="post" action="signup-process.php">
            <div class="col-lg-6">
                <div class="well-well-sm"><strong><span class="glyphicon-asterisk"></span>Required
                        Field</strong></div>

                <div class="form-group">
                    <label for="InputName">Username*</label>

                    <div class="input-group">
                        <input type="text" name="uname" class="form-control" name="InputName" id="InputName"
                               placeholder="Username" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>                        
                    </div>
                </div>

                <div class="form-group">
                    <label for="InputName">Password*</label>

                    <div class="input-group">
                        <input type="text" name="pw1" class="form-control" name="InputName" id="InputName"
                               placeholder="Password" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>                       
                    </div>
                </div>

                <div class="form-group">
                    <label for="InputName">Confirm Password*</label>

                    <div class="input-group">
                        <input type="text" name="pw2" class="form-control" name="InputName" id="InputName"
                               placeholder="Confirm Password" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>                   
                    </div>
                </div>

                <div class="form-group">
                    <label for="InputName">Enter Email*</label>

                    <div class="input-group">
                        <input type="email" name="email" class="form-control" name="InputName" id="InputName"
                               placeholder="Enter Email" required>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>                   
                    </div>
                </div>

                <input type="submit" name="submit" id="submit" value="submit" class="btn btn-info pull-right">

            </div>
        </form>
        <div class="col-lg-5 col-md-push-1">
            <div class="col-md-12">
            </div>
        </div>
    </div>
    <! Registration form - END -->
</div>
