<?php
include_once "../vendor/autoload.php";


use App\Users\Users;

$obj = new Users();
$obj->prepare($_GET);
//
//$onedata = $obj->show();
//echo "<pre>";
//print_r($_SESSION['Login_data']);
//echo "</pre>";


if (isset($_SESSION['Login_data']) && !empty($_SESSION['Login_data'])) {
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Data Update Form</title>
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">

        <!-- JS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <!--contact-form-->
    <?php include_once "include/log-navbar.php"; ?>
    <div class="contact-form">

        <div class="container">

            <div class="row">
                <div class="col-md-5 col-md-offset-3">
                    <h3>Profile Update Form</h3>
                    <strong class="text-success"><?php $obj->Validation("Pro_U") ?></strong>

                    <form action="profile-process.php" method="post" enctype="multipart/form-data">

                        <label>Full Name</label>
                        <input class="form-control" type="text" name="full_name" value="">
                        <p class="text-danger"><?php $obj->Validation("full_name_R"); ?></p><br>

                        <label>Father Name</label>
                        <input class="form-control" type="text" name="father_name" value="">
                        <p class="text-danger"><?php $obj->Validation("father_name_R"); ?></p><br>

                        <label>Mother Name</label>
                        <input class="form-control" type="text" name="mother_name" value="">
                        <p class="text-danger"><?php $obj->Validation("mother_name_R"); ?></p><br>

                        <label>Update your gender: </label>
                        <label for="male">
                            <input type="radio" name="gender"
                                   value="Male" <?php //echo ($singledata["gender"] == 'Male') ? 'checked' : '' ?>
                                   id="male">
                            Male
                        </label>
                        <label for="female">
                            <input type="radio" name="gender"
                                   value="Female" <?php //echo ($singledata["gender"] == 'Female') ? 'checked' : '' ?>
                                   id="female">
                            Female
                        </label>
                        <p class="text-danger"><?php $obj->Validation("gender_R"); ?></p><br><br>

                        <label>Phone: </label>
                        <input type="number" name="phone" class="form-control"
                               value="<?php //echo $singledata['phone_number']?>">
                        <p class="text-danger"><?php $obj->Validation("phone_R"); ?></p><br>

                        <label>FAX Number</label>
                        <input type="number" name="fax_number" class="form-control"
                               value="<?php //echo $singledata['phone_number']?>">
                        <p class="text-danger"><?php $obj->Validation("fax_number_R"); ?></p><br>

                        <label>Web Address</label>
                        <input type="url" name="web_address" class="form-control"
                               value="<?php //echo $singledata['phone_number']?>">
                        <p class="text-danger"><?php $obj->Validation("web_address_R"); ?></p><br>

                        <label>Date of Birth: </label>
                        <input type="date" name="dateofbirth" class="form-control"
                               value="<?php //echo $singledata['dates']?>">
                        <p class="text-danger"><?php $obj->Validation("dateofbirth_R"); ?></p><br>

                        <label>Height</label>
                        <input class="form-control" type="text" name="height" value="">
                        <p class="text-danger"><?php $obj->Validation("height"); ?></p><br>

                        <label>Occupation</label>
                        <input class="form-control" type="text" name="occupation" value="">
                        <p class="text-danger"><?php $obj->Validation("occupation"); ?></p><br>

                        <label>Education Status</label>
                        <input class="form-control" type="text" name="education_status" value="">
                        <p class="text-danger"><?php $obj->Validation("education_status"); ?></p><br>

                        <label>Religion</label>
                        <input class="form-control" type="text" name="religion" value="">
                        <p class="text-danger"><?php $obj->Validation("religion"); ?></p><br>

                        <label>Marital Status</label>
                        <input class="form-control" type="text" name="marital_status" value="">
                        <p class="text-danger"><?php $obj->Validation("marital_status"); ?></p><br>

                        <label>Current Job Status</label>
                        <input class="form-control" type="text" name="current_job" value="">
                        <p class="text-danger"><?php $obj->Validation("current_job"); ?></p><br>

                        <label>Nationality</label>
                        <input class="form-control" type="text" name="nationality" value="">
                        <p class="text-danger"><?php $obj->Validation("nationality"); ?></p><br>

                        <label>Interested: </label>
                        <label>
                            <input type="checkbox" name="interested[]"
                                   value="Reading" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Reading </label>
                        <label>
                            <input type="checkbox" name="interested[]"
                                   value="Traveling" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Traveling </label>
                        <label>
                            <input type="checkbox" name="interested[]"
                                   value="Blogging" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Blogging </label>
                        <label>
                            <input type="checkbox" name="interested[]"
                                   value="Collecting" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Collecting </label>
                        <p class="text-danger"><?php $obj->Validation("interested"); ?></p><br><br>

                        <label>Bio</label>
                        <textarea class="form-control" name="bio" rows="5"></textarea>
                        <p class="text-danger"><?php $obj->Validation("bio"); ?></p><br>

                        <label>NID</label>
                        <input type="number" name="nid" class="form-control"
                               value="<?php //echo $singledata['phone_number']?>">
                        <p class="text-danger"><?php $obj->Validation("nid"); ?></p><br>

                        <label>passport Number</label>
                        <input type="number" name="passport_number" class="form-control"
                               value="<?php //echo $singledata['phone_number']?>">
                        <p class="text-danger"><?php $obj->Validation("passport_number"); ?></p><br>

                        <label>Skills Area: </label><br>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Accuracy" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Accuracy </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Budgeting" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Budgeting </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Calculating_data" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Calculating data </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Defining_problems" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Defining problems </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Editing" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Editing </label>
                        <label>
                            <input type="checkbox" name="skills_area[]"
                                   value="Initiator" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Initiator </label>
                        <p class="text-danger"><?php $obj->Validation("skills_area"); ?></p><br><br>

                        <label>Language Area: </label><br>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="English" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            English </label>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="French" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> > French
                        </label>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="German" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> > German
                        </label>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="Japanese" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> >
                            Japanese </label>
                        <label>
                            <input type="checkbox" name="language_area[]"
                                   value="Bangle" <?php //echo ($ondata["offer"] == 'Yes') ? 'checked' : '' ?> > Bangle
                        </label>
                        <p class="text-danger"><?php $obj->Validation("language_area"); ?></p><br><br>

                        <label>Blood Group: </label>
                        <select name="Blood_group" class="form-control">
                            <option selected disabled hidden>Select Here</option>
                            <option
                                value="A+" <?php //echo ($singledata['city'] == "Chittagong") ? 'selected' : ''; ?> >A+
                            </option>
                            <option value="O+" <?php //echo ($singledata['city'] == "Dhaka") ? 'selected' : ''; ?> >
                                O+
                            </option>
                            <option
                                value="B+" <?php //echo ($singledata['city'] == "Barisal") ? 'selected' : ''; ?> >
                                B+
                            </option>
                            <option
                                value="AB+" <?php //echo ($singledata['city'] == "Khulna") ? 'selected' : ''; ?> >
                                AB+
                            </option>
                            <option
                                value="A-" <?php //echo ($singledata['city'] == "Rajshahi") ? 'selected' : ''; ?> >
                                A-
                            </option>
                            <option
                                value="O-" <?php //echo ($singledata['city'] == "Rangpur") ? 'selected' : ''; ?> >
                                O-
                            </option>
                            <option
                                value="B-" <?php //echo ($singledata['city'] == "Sylhet") ? 'selected' : ''; ?> >
                                B-
                            </option>
                            <option
                                value="AB-" <?php //echo ($singledata['city'] == "Sylhet") ? 'selected' : ''; ?> >
                                AB-
                            </option>
                        </select>
                        <p class="text-danger"><?php $obj->Validation("Blood_group"); ?></p><br><br>


                        <label>Address</label>
                        <input class="form-control" type="text" name="address_one" value="" placeholder="Address one">
                        <p class="text-danger"><?php $obj->Validation("address_one"); ?></p><br>

                        <input class="form-control" type="text" name="address_two" value=""
                               placeholder="Address two"><br>

                        <input class="form-control" type="number" name="post" value="" placeholder="Post code">
                        <p class="text-danger"><?php $obj->Validation("post"); ?></p><br>

                        <input class="form-control" type="text" name="state" value="" placeholder="State">
                        <p class="text-danger"><?php $obj->Validation("state"); ?></p><br>

                        <input class="form-control" type="text" name="city" value="" placeholder="City">
                        <p class="text-danger"><?php $obj->Validation("city"); ?></p><br>

                        <label>Others</label>
                        <input class="form-control" type="text" name="others"><br>

                        <label>Profile Pic</label>
                        <input type="file" name="photo">
                        <!--                        --><?php
                        //                        if($row['image'] == ""){
                        //                            echo "<img width='100' height='80' src='profile_pic/defult-pic.png' alt='Default Profile Pic'>";
                        //                        } else {
                        //                            echo "<img width='100' height='100' src='profile_pic/".$row['image']."' alt='Profile Pic'>";
                        //                        }
                        //                        ?>
                        <p class="text-danger"><?php $obj->Validation("photo"); ?></p><br>
                        <p class="text-danger"><?php $obj->Validation("profile_pic"); ?></p><br>


                        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                        <input class="btn btn-block" type="submit" value="Update" name="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/contact-form-->
    </body>
    </html>
    <?php
} else {
    $_SESSION['Errors_R'] = "User not found :(";
    header("location:errors.php");
}
?>

