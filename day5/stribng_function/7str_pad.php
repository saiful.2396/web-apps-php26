<?php

$str = "Beautifull Bangladesh";
echo str_pad($str, 25," Dot");
echo '<br/>';

//str_repeat
echo str_repeat('done ',3);
echo "<br/>";

//str_replace
echo str_replace("world", "Peter", "I love world & my Bangladesh.");

//str_split
$str ="Hello world";
$data = str_split($str,6);
echo '<pre>';
print_r($data);

//strp_tag
$str = "Hello <b> Bangladesh</b>";
echo $str;
echo '<br/>';

echo strip_tags($str);
