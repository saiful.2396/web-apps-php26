<?php
include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $obj->prepare($_POST);
    $obj->Verification();

    if (!empty($_POST['usname']) && !empty($_POST['password']) && !empty($_POST['email'])) {
        $to = $_POST['email'];
        $subject = 'Signup | Verification';
        $message = '
        Thanks for signing up!
        Activated your account by pressing the url below.

        ------------------------
        Username: ' . $_POST['usname'] . '
        Password: ' . $_POST['password'] . '
        ------------------------

        Please click this link to activate your account:
        http://www.themeyellow.com/testing/crud/views/verify.php?vid=' . $_SESSION['vid'] . '';
        $message = wordwrap($message,70);
        $headers = 'From:noreply@themeyellow.com' . "\r\n";

        @mail($to, $subject, $message, $headers);
    }

    $obj->store();

} else {
    $_SESSION['Errors_R'] = "404 not found :(";
    header("location:errors.php");
}

?>