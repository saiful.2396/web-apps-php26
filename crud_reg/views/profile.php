<?php
include_once "../vendor/autoload.php";


use App\Users\Users;

$obj = new Users();
$obj->prepare($_GET);

$onedata = $obj->profileShow();

$interesteds = explode(",", $onedata['interested']);
$skills_area = explode(",", $onedata['skills_area']);
$language_area = explode(",", $onedata['language_area']);

$add = unserialize($onedata['address']);
//$imageex = end(explode('.', $onedata['profile_pic']));

$showdata = $obj->show();

//echo "<pre>";
//print_r($showdata);
//print_r($language_area);
//////print_r($add);
//echo "</pre>";


if (isset($_SESSION['Login_data']) && !empty($_SESSION['Login_data'])) {
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Data Update Form</title>
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">

        <!-- JS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <!--contact-form-->
    <?php include_once "include/log-navbar.php"; ?>
    <div class="contact-form">

        <div class="container">

            <div class="row">
                <div class="col-md-5 col-md-offset-3">
                    <h3>Profile Update Form</h3>
                    <strong class="text-success"><?php $obj->Validation("Pro_U") ?></strong>

                    <form action="profile-process.php" method="post" enctype="multipart/form-data">

                        <label>Full Name</label>
                        <input class="form-control" type="text" name="full_name"
                               value="<?php echo $onedata['full_name']; ?>">
                        <p class="text-danger"><?php $obj->Validation("full_name_R"); ?></p><br>

                        <label>Father Name</label>
                        <input class="form-control" type="text" name="father_name"
                               value="<?php echo $onedata['father_name']; ?>">
                        <p class="text-danger"><?php $obj->Validation("father_name_R"); ?></p><br>

                        <label>Mother Name</label>
                        <input class="form-control" type="text" name="mother_name"
                               value="<?php echo $onedata['mother_name']; ?>">
                        <p class="text-danger"><?php $obj->Validation("mother_name_R"); ?></p><br>

                        <label>Update your gender: </label>
                        <label for="male">
                            <input type="radio" name="gender"
                                   value="Male" <?php echo ($onedata["gender"] == 'Male') ? 'checked' : '' ?>
                                   id="male">
                            Male
                        </label>
                        <label for="female">
                            <input type="radio" name="gender"
                                   value="Female" <?php echo ($onedata["gender"] == 'Female') ? 'checked' : '' ?>
                                   id="female">
                            Female
                        </label>
                        <p class="text-danger"><?php $obj->Validation("gender_R"); ?></p><br><br>

                        <label>Phone: </label>
                        <input type="number" name="phone" class="form-control"
                               value="<?php echo $onedata['phone'] ?>">
                        <p class="text-danger"><?php $obj->Validation("phone_R"); ?></p><br>

                        <label>FAX Number</label>
                        <input type="number" name="fax_number" class="form-control"
                               value="<?php echo $onedata['fax_number'] ?>">
                        <p class="text-danger"><?php $obj->Validation("fax_number_R"); ?></p><br>

                        <label>Web Address</label>
                        <input type="url" name="web_address" class="form-control"
                               value="<?php echo $onedata['web_address'] ?>">
                        <p class="text-danger"><?php $obj->Validation("web_address_R"); ?></p><br>

                        <label>Date of Birth: </label>
                        <input type="date" name="dateofbirth" class="form-control"
                               value="<?php echo $onedata['dateofbirth'] ?>">
                        <p class="text-danger"><?php $obj->Validation("dateofbirth_R"); ?></p><br>

                        <label>Height</label>
                        <input class="form-control" type="text" name="height"
                               value="<?php echo $onedata['height'] ?>">
                        <p class="text-danger"><?php $obj->Validation("height"); ?></p><br>

                        <label>Occupation</label>
                        <input class="form-control" type="text" name="occupation"
                               value="<?php echo $onedata['occupation'] ?>">
                        <p class="text-danger"><?php $obj->Validation("occupation"); ?></p><br>

                        <label>Education Status</label>
                        <input class="form-control" type="text" name="education_status"
                               value="<?php echo $onedata['education_status'] ?>">
                        <p class="text-danger"><?php $obj->Validation("education_status"); ?></p><br>

                        <label>Religion</label>
                        <input class="form-control" type="text" name="religion"
                               value="<?php echo $onedata['religion'] ?>">
                        <p class="text-danger"><?php $obj->Validation("religion"); ?></p><br>

                        <label>Marital Status</label>
                        <input class="form-control" type="text" name="marital_status"
                               value="<?php echo $onedata['marital_status'] ?>">
                        <p class="text-danger"><?php $obj->Validation("marital_status"); ?></p><br>

                        <label>Current Job Status</label>
                        <input class="form-control" type="text" name="current_job"
                               value="<?php echo $onedata['current_job'] ?>">
                        <p class="text-danger"><?php $obj->Validation("current_job"); ?></p><br>

                        <label>Nationality</label>
                        <input class="form-control" type="text" name="nationality"
                               value="<?php echo $onedata['nationality'] ?>">
                        <p class="text-danger"><?php $obj->Validation("nationality"); ?></p><br>

                        <label>Interested: </label><br>
                        <?php
                        //print_r($interesteds);
                        $inarray = array('Reading', 'Traveling', 'Blogging', 'Collecting');

                        foreach ($inarray as $inval) {
                            if (in_array($inval, $interesteds)) {
                                echo "<label><input type='checkbox' name='interested[]'value='" . $inval . "' checked /> " . $inval . " </label>";
                            } else {
                                echo "<label><input type='checkbox' name='interested[]'value='" . $inval . "' /> " . $inval . "  </label>";

                            }
                        }

                        ?><br>
                        <p class="text-danger"><?php $obj->Validation("interested"); ?></p><br><br>

                        <label>Bio</label>
                        <textarea class="form-control" name="bio" rows="5"><?php echo $onedata['bio'] ?></textarea>
                        <p class="text-danger"><?php $obj->Validation("bio"); ?></p><br>

                        <label>NID</label>
                        <input type="number" name="nid" class="form-control"
                               value="<?php echo $onedata['nid'] ?>">
                        <p class="text-danger"><?php $obj->Validation("nid"); ?></p><br>

                        <label>passport Number</label>
                        <input type="number" name="passport_number" class="form-control"
                               value="<?php echo $onedata['passport_number'] ?>">
                        <p class="text-danger"><?php $obj->Validation("passport_number"); ?></p><br>
                        <label>Skills Area: </label><br>
                        <?php
                        //print_r($skills_area);
                        $skiarray = array('Accuracy', 'Budgeting', 'Calculating_data', 'Defining_problems', 'Editing', 'Initiator');
                        foreach ($skiarray as $skinval) {
                            if (in_array($skinval, $skills_area)) {
                                echo "<label><input type='checkbox' name='skills_area[]' value='" . $skinval . "' checked /> " . $skinval . " </label>";
                            } else {
                                echo "<label><input type='checkbox' name='skills_area[]' value='" . $skinval . "' /> " . $skinval . "  </label>";

                            }
                        }
                        ?><br>
                        <p class="text-danger"><?php $obj->Validation("skills_area"); ?></p><br><br>

                        <label>Language Area: </label><br>
                        <?php
                        //print_r($language_area);
                        $lanarray = array('English', 'French', 'German', 'Japanese', 'Bangle');
                        foreach ($lanarray as $lannval) {
                            if (in_array($lannval, $language_area)) {
                                echo "<label><input type='checkbox' name='language_area[]' value='" . $lannval . "' checked /> " . $lannval . " </label>";
                            } else {
                                echo "<label><input type='checkbox' name='language_area[]' value='" . $lannval . "' /> " . $lannval . "  </label>";

                            }
                        }
                        ?><br>
                        <p class="text-danger"><?php $obj->Validation("language_area"); ?></p><br><br>

                        <label>Blood Group: </label>
                        <select name="Blood_group" class="form-control">
                            <option selected disabled hidden>Select Here</option>
                            <option
                                value="A+" <?php echo ($onedata['Blood_group'] == "A+") ? 'selected' : ''; ?> >A+
                            </option>
                            <option value="O+" <?php echo ($onedata['Blood_group'] == "O+") ? 'selected' : ''; ?> >
                                O+
                            </option>
                            <option
                                value="B+" <?php echo ($onedata['Blood_group'] == "B+") ? 'selected' : ''; ?> >
                                B+
                            </option>
                            <option
                                value="AB+" <?php echo ($onedata['Blood_group'] == "AB+") ? 'selected' : ''; ?> >
                                AB+
                            </option>
                            <option
                                value="A-" <?php echo ($onedata['Blood_group'] == "A-") ? 'selected' : ''; ?> >
                                A-
                            </option>
                            <option
                                value="O-" <?php echo ($onedata['Blood_group'] == "O-") ? 'selected' : ''; ?> >
                                O-
                            </option>
                            <option
                                value="B-" <?php echo ($onedata['Blood_group'] == "B-") ? 'selected' : ''; ?> >
                                B-
                            </option>
                            <option
                                value="AB-" <?php echo ($onedata['Blood_group'] == "AB-") ? 'selected' : ''; ?> >
                                AB-
                            </option>
                        </select>
                        <p class="text-danger"><?php $obj->Validation("Blood_group"); ?></p><br><br>


                        <label>Address</label>
                        <input class="form-control" type="text" name="address_one"
                               value="<?php echo $add["address_one"]; ?>" placeholder="Address one">
                        <p class="text-danger"><?php $obj->Validation("address_one"); ?></p><br>

                        <input class="form-control" type="text" name="address_two"
                               value="<?php echo $add["address_two"]; ?>"
                               placeholder="Address two"><br>

                        <input class="form-control" type="number" name="post" value="<?php echo $add["post"]; ?>"
                               placeholder="Post code">
                        <p class="text-danger"><?php $obj->Validation("post"); ?></p><br>

                        <input class="form-control" type="text" name="state" value="<?php echo $add["state"]; ?>"
                               placeholder="State">
                        <p class="text-danger"><?php $obj->Validation("state"); ?></p><br>

                        <input class="form-control" type="text" name="city" value="<?php echo $add["city"]; ?>"
                               placeholder="City">
                        <p class="text-danger"><?php $obj->Validation("city"); ?></p><br>

                        <label>Others</label>
                        <input class="form-control" type="text" name="others" value="<?php echo $onedata["others"]; ?>"><br>

                        <label>Profile Pic</label>
                        <input type="file" name="photo">
                        <?php
//                        print_r($onedata['profile_pic']);
                        if ($onedata['profile_pic'] == "") {
                            echo "<img style='display: block;float: right;margin-right: 140px;margin-top: -50px;margin-bottom: 50px;border: 1px solid #ccc;padding: 2px;' width='100' height='100' src='photo/defult-pic.png' alt='Default Profile Pic'>";
                        } else {
                            echo "<img style='display: block;float: right;margin-right: 140px;margin-top: -40px;margin-bottom: 50px;border: 1px solid #ccc;padding: 2px;'  width='100' height='100' src='photo/" . $onedata['profile_pic'] . "' alt='Profile Pic'>";
                        }
                        ?>
                        <p class="text-danger"><?php $obj->Validation("photo_extension"); ?></p><br>
                        <p class="text-danger"><?php $obj->Validation("photo_size"); ?></p><br>


                        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                        <input class="btn btn-block" type="submit" value="Update" name="submit">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/contact-form-->
    </body>
    </html>
    <?php
} else {
    $_SESSION['Errors_R'] = "User not found :(";
    header("location:errors.php");
}
?>

