<?php
include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();
$obj->prepare($_GET);

$showdata = $obj->show();

//echo "<pre>";
//print_r($showdata);
//echo "</pre>";

if (isset($_SESSION['Login_data']) && !empty($_SESSION['Login_data'])) {
    ?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Data Update Form</title>
        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">

        <!-- JS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <!--contact-form-->
    <?php include_once "include/log-navbar.php"; ?>
    <div class="contact-form">

        <div class="container">

            <div class="row">
                <div class="col-md-5 col-md-offset-3">
                    <h3>Data Update Form</h3>
                    <strong class="text-success"><?php $obj->Validation("Up_M") ?></strong>

                    <form action="pro-update-process.php" method="post">
                        <label>Change User Name</label>
                        <input class="form-control" type="text" name="usname" value="<?php echo $showdata['user_name']; ?>">
                        <p class="text-danger"><?php $obj->Validation("US_N"); ?></p><br>

                        <label>Change Password</label>
                        <input class="form-control" type="password" name="password" value="<?php echo $showdata['password']; ?>">
                        <p class="text-danger"><?php $obj->Validation("US_P"); ?></p><br>


                        <label>Email</label>
                        <input class="form-control" type="email" name="email" value="<?php echo $showdata['email']; ?>">
                        <p class="text-danger"><?php $obj->Validation("U_EM"); ?></p><br>

                        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                        <input class="btn btn-block" type="submit" value="Update">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--/contact-form-->
    </body>
    </html>
    <?php
} else {
    $_SESSION['Errors_R'] = "User not found :(";
    header("location:errors.php");
}
?>

