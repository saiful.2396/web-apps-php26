<nav class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php?id=<?php echo $_SESSION['Login_data']['id'];?>"">Yellow</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <?php if($_SESSION['Login_data']['is_admin'] == 1){;?>
                    <li><a href="trash-item.php?id=<?php echo $_SESSION['Login_data']['id'];?>">Trash Items </a></li>
                <?php }?>
                <li><a href="profile.php?id=<?php echo $_SESSION['Login_data']['id'];?>">profile </a></li>
                <li><a href="pro-update.php?id=<?php echo $_SESSION['Login_data']['id'];?>"> Account </a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true"
                       aria-expanded="false">
                        <?php
                        if ($_SESSION['pro_data']['profile_pic'] == "") {
                            echo "<img style='display: inline-block;border: 1px solid #ccc;padding: 1px;width: 40px;height: 40px;' src='../photo/defult-pic.png' alt='Default Profile Pic'>";
                        } else {
                            echo "<img style='display: inline-block;border: 1px solid #ccc;padding: 1px;width: 40px;height: 40px;' src='photo/" . $_SESSION['pro_data']['profile_pic'] . "' alt='Profile Pic'>";
                        }
                        ?>

                        <?php echo $_SESSION['acc_data']['user_name'];?>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="logout.php">Logout</a></li>
                    </ul>
                </li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>