<?php
include_once "../vendor/autoload.php";

use App\Users\Users;

$obj = new Users();
$obj->prepare($_GET);

$alldata = $obj->profileIndex();
$showdata = $obj->show();

//echo "<pre>";
//print_r($showdata);
//echo "</pre>";

if (isset($_SESSION['Login_data']) && !empty($_SESSION['Login_data'])) {
    ?>

    <html>
    <head>
        <title>Index | Page</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
    <?php include_once "include/log-navbar.php"; ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top: 50px">
                <h4 class="text-success"><?php $obj->Validation("Login"); ?></h4>
                <?php
                if ($_SESSION['Login_data']['is_admin'] == 1) {

                    ?>
                    <h3 class="text-center">User List</h3>
                    <strong class="text-success"><?php $obj->Validation("Tr_D") ?></strong>

                    <table class="table table-bordered table-hover mtop">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>ID</th>
                            <th>User ID</th>
                            <th>User Name</th>
                            <th>password</th>
                            <th>email</th>
                            <th>is_active</th>
                            <th>is_admin</th>
                            <th>is_delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            $serial = 1;
                            if (isset($alldata) && !empty($alldata)) {
                            foreach ($alldata as $onedata) {
                            ?>
                            <td><?php echo $serial++; ?></td>
                            <td><?php echo $onedata['id'] ?></td>
                            <td><?php echo $onedata['users_id'] ?></td>
                            <td><?php echo $onedata['user_name'] ?></td>
                            <td><?php echo $onedata['password'] ?></td>
                            <td><?php echo $onedata['email'] ?></td>
                            <td><?php echo $onedata['is_active'] ?></td>
                            <td><?php echo $onedata['is_admin'] ?></td>
                            <td><?php echo $onedata['is_delete'] ?></td>
                        </tr>
                        <?php }
                        } ?>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-hover mtop">
                        <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Father Name</th>
                            <th>Mother Name</th>
                            <th>Gender</th>
                            <th>Phone</th>
                            <th>fax_number</th>
                            <th>web_address</th>
                            <th>date of birth</th>
                            <th>height</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            if (isset($alldata) && !empty($alldata)) {
                            foreach ($alldata as $onedata) {
                            ?>
                            <td><?php echo $onedata['full_name'] ?></td>
                            <td><?php echo $onedata['father_name'] ?></td>
                            <td><?php echo $onedata['mother_name'] ?></td>
                            <td><?php echo $onedata['gender'] ?></td>
                            <td><?php echo $onedata['phone'] ?></td>
                            <td><?php echo $onedata['fax_number'] ?></td>
                            <td><?php echo $onedata['web_address'] ?></td>
                            <td><?php echo $onedata['dateofbirth'] ?></td>
                            <td><?php echo $onedata['height'] ?></td>
                        </tr>
                        <?php }
                        } ?>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-hover mtop">
                        <thead>
                        <tr>
                            <th>occupation</th>
                            <th>education_status</th>
                            <th>religion</th>
                            <th>marital_status</th>
                            <th>current_job</th>
                            <th>nationality</th>
                            <th>interested</th>
                            <th>bio</th>
                            <th>nid</th>
                            <th>passport_number</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            if (isset($alldata) && !empty($alldata)) {
                            foreach ($alldata as $onedata) {
                            ?>
                            <td><?php echo $onedata['occupation'] ?></td>
                            <td><?php echo $onedata['education_status'] ?></td>
                            <td><?php echo $onedata['religion'] ?></td>
                            <td><?php echo $onedata['marital_status'] ?></td>
                            <td><?php echo $onedata['current_job'] ?></td>
                            <td><?php echo $onedata['nationality'] ?></td>
                            <td><?php echo $onedata['interested'] ?></td>
                            <td><?php echo $onedata['bio'] ?></td>
                            <td><?php echo $onedata['nid'] ?></td>
                            <td><?php echo $onedata['passport_number'] ?></td>
                        </tr>
                        <?php }
                        } ?>
                        </tbody>
                    </table>

                    <table class="table table-bordered table-hover mtop">
                        <thead>
                        <tr>
                            <th>skills_area</th>
                            <th>language_area</th>
                            <th>Blood_group</th>
                            <th>address</th>
                            <th>others</th>
                            <th>profile_pic</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <?php
                            if (isset($alldata) && !empty($alldata)) {
                            foreach ($alldata as $onedata) {
                            ?>
                            <td><?php echo $onedata['skills_area'] ?></td>
                            <td><?php echo $onedata['language_area'] ?></td>
                            <td><?php echo $onedata['Blood_group'] ?></td>
                            <td><?php $addata = unserialize($onedata['address']);
                                echo $addata['address_one'] . ", ";
                                echo $addata['address_two'] . ", ";
                                echo $addata['post'] . ", ";
                                echo $addata['state'] . ", ";
                                echo $addata['city'] . ".";
                                ?></td>
                            <td><?php echo $onedata['others'] ?></td>
                            <td><?php echo "<img style='border: 1px solid #ccc;padding: 2px;' width='100' height='100' src='photo/" . $onedata['profile_pic'] . "' alt='Profile Pic'>"; ?></td>
                        </tr>
                        <?php }
                        } ?>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>created</th>
                            <th>modified</th>
                            <th>deleted</th>
                            <th>restore</th>

                            <th colspan="3" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if (isset($alldata) && !empty($alldata)) {
                            foreach ($alldata as $onedata) {
                                ?>
                                <td><?php echo $onedata['created'] ?></td>
                                <td><?php echo $onedata['modified'] ?></td>
                                <td><?php echo $onedata['deleted'] ?></td>
                                <td><?php echo $onedata['restore'] ?></td>

                                <td><a href="profile2.php?id=<?php echo $onedata['users_id'] ?>">View</a></td>
                                <td><a href="profile.php?id=<?php echo $onedata['users_id'] ?>">Edit</a></td>
                                <td><?php if ($onedata['is_admin'] == 0) { ?>
                                        <a href="trash.php?id=<?php echo $onedata['users_id'] ?>">Delete</a>
                                    <?php } ?>
                                </td>

                                </tr>
                            <?php }
                        } else { ?>
                            <tr>
                                <td colspan="9">No available data</td>
                            </tr>

                        <?php } ?>

                        </tbody>
                    </table>
                    <?php
                } else {
                    echo "<h1 class='text-center'>"."Welcome to Dashboard"."</h1>";
                }
                    ?>
            </div>
        </div>
    </div>
    </body>
    </html>
    <?php
} else {
    $_SESSION['Errors_R'] = "User not found :(";
    header("location:errors.php");
}
?>
