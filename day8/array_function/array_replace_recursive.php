<?php
$arr = array("a" => array("Green"), "b" => array("Red", "Blue"));
echo "<pre>";
print_r($arr);

$arr2 = array("a" => array("Black"), "b" => array("Yellow"));
echo "<pre>";
print_r($arr2);

$data = array_replace_recursive($arr,$arr2);

echo "<pre>";
print_r($data);