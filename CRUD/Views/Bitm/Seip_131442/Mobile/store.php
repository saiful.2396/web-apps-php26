<?php
include_once ("../../../../vendor/autoload.php");
use Apps\Bitm\Seip_131442\Mobile\Mobile;
$obj = new Mobile();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $obj->prepare($_POST);
    $obj->store();
} else {
    $_SESSION['Err_Msg'] = "Oops ! You are not authorized to access this page";
    header('location:error.php');
}