<?php

include_once ("../../../../vendor/autoload.php");
use Apps\Bitm\Seip_131442\Mobile\Mobile;

$obj=new Mobile;

$pdf=$obj->index();
$trs="";
$serial=0;
foreach ($pdf as $data):
    $serial++;
    $trs.="<tr>";
    $trs.="<td>".$serial."</td>";
    $trs.="<td>".$data['id']."</td>";
    $trs.="<td>".$data['title']."</td>";
    $trs.="</tr>";
endforeach;
$html=<<<EOD
    <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>List of Mobile Models</title>
</head>
<body>
    <h1>List of Mobiles</h1>
    <table>
        <thead>
            <tr>
                <th>Sl.</th>
                <th>ID.</th>
                <th>Title</th>
            </tr>
        </thead>
        <tbody>
            $trs;
        </tbody>
    </table>
</body>
</html>
EOD;

$mpdf=new mPDF();
$mpdf->WriteHTML($html);
$mpdf->Output();
exit;
?>
