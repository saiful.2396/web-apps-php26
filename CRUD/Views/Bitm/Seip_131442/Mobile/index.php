<?php
include_once ("../../../../vendor/autoload.php");
?>
<a href="../../../../index.php">List of Project</a>
<a href="create.php">Add New Model</a>

<?php

use Apps\Bitm\Seip_131442\Mobile\Mobile;

$obj = new Mobile();
$data = $obj->index();
echo "<br/>";
if (isset($_SESSION['Message']) && !empty($_SESSION['Message'])) {
    echo $_SESSION['Message'];
    unset($_SESSION['Message']);
}
?>
<html>
    <head>
        <title>Index | Data</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <th>ID</th>
                <th>Mobile_Model</th>
                <th>Laptop_Model</th>
                <th colspan="3">Action</th>
            </tr>
            <?php
            $serial = 1;
            if (isset($data) && !empty($data)) {
                foreach ($data as $item) {
                    ?>
                    <tr>
                        <td><?php echo $serial++ ?></td>
                        <td><?php echo $item['title'] ?></td>
                        <td><?php echo $item['laptop'] ?></td>
                        <td><a href="show.php?id=<?php echo $item['unique_id']; ?>">View</a></td>
                        <td><a href="edit.php?id=<?php echo $item['unique_id']; ?>">Edit</a></td>
                        <td><a href="delete.php?id=<?php echo $item['unique_id']; ?>">Delete</a></td>
                    </tr>
    <?php
    }
} else {
    ?>
                <tr>
                    <td colspan="3">No available Data</td>
                </tr>
<?php } ?>

        </table>

        <h1>List of Mobiles</h1>
        <div>
<?php //echo substr($paging,0,strlen($paging)-2) ?>
        </div>
        <!--    <a href="trashed.php">All Trashed Data</a>-->

        <div><span>Search / Filter</span>
            <span id="utility">Download as <a href="pdf.php">PDF</a> | <a href="xl.php">Xl</a> | <a href="create.php">Create New</a></span>
            <form action="index.php">
                <select name="itemPerPage" id="">
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="30">30</option>
                    <option value="40">40</option>
                    <option value="50">60</option>
                </select>
                <button type="submit">Go</button>
            </form>
        </div>
    </body>

</html>
