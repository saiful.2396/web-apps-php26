<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/London');

if(PHP_SAPI == 'cli')
    die('This example should only be run from a Web Browser');
/* Include PHPEXcel*/
include "../../../../vendor/mpdf/mpdf/mpdf.php";

include_once "../../../../vendor/autoload.php";
use Apps\Bitm\Seip_131442\Mobile\Mobile;
$obj = new Mobile();
$mydata = $obj->index();

/*Create new PHPExcel object*/
$objPHPExcel = new PHPExcel();
/* set document properties */

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
        ->SetLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 SLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");
// Add some data
$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'Si')
        ->setCellValue('B1', 'Id')
        ->setCellValue('C1', 'Title')
        ->setCellValue('E1' , 'Unique_Id');

$counter=2;
$serial =0;

foreach($mydata as $data){
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A'.$counter, $serial)
            ->setCellValue('B'.$counter, $data['id'])
            ->setCellValue('C'.$counter, $data['title'])
            ->setCellValue('E'.$counter, $data['unique_id']);
    $counter++;
            
}

// Rename vorksheet
$objPHPExcel->getActiveSheet()->setTitle("Name_List");

// set active sheet index to the first sheet, so excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

//Redirect output to a client's web browser (Excel15)

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=01simple.xls');
header('Cache-Control:max-age=0');
// if you are serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// if you are serving to IE over SSL, then the following may be needed

header('Expires:Mon,26 Jul 1997 05:00:00 GMT');// Date in the past
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');//always modified
header('Cache-Control: cache, must-revalidate'); //HTTP/1.1
header('Pragma: public'); // HTTp/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;


?>