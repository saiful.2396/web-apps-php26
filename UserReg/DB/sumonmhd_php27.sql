-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 27, 2016 at 08:58 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sumonmhd_php27`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `verification` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `verification`, `username`, `password`, `email`, `is_active`, `is_admin`) VALUES
(8, '5794f173cf4fc', '5794f173cf4ef', 'muktamhds', '25121990', 'cse.abutaleb@gmail.com', 1, 0),
(7, 'fsdfsdf', 'sfdsfsfs', 'sfdsf', '25121990', 'sdfsf@c.com', 0, 0),
(6, '5794e631ef4d4', '5794e631ef4c4', 'sumonmhd', '25121990', 'mukta.jes.bd@gmail.com', 0, 0),
(5, '5794d817274b1', '5794d8172749b', 'muktamhd', '25121990', 'fake.account@innocent.com', 1, 0),
(9, '5794fc9fd8edf', '5794fc9fd8ec1', 'muktamhdd', '25121990', 'fake.accofunt@innocent.com', 1, 0),
(10, '57951226e77e0', '57951226e77c5', 'abcdefg', '25121990', 'fafke.daccount@innocent.com', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
