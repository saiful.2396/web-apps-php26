<?php
namespace App\users;

use PDO;

class users
{
    public $dbuser = 'root';
    public $dbpass = '';
    public $username = '';
    public $password = '';
    public $email = '';
    public $conn = '';
    public $data = '';
    public $userexist = '';
    public $verification_id = '';
    public $first_name='';
    public $last_name = '';
    public $id='';
    public $image_name = '';
    public $allUsers = '';

    public function __construct()
    {
        session_start();
        $this->conn = new PDO('mysql:host=localhost;dbname=php26', $this->dbuser, $this->dbpass);
    }

    public function prepare($data = '')
    {
//
        if (!empty($data['id'])) {
            $this->id = $data['id'];
        }
        if (!empty($data['uname'])) {
            $this->username = $data['uname'];
        }

        if (!empty($data['pw1'])) {
            $this->password = $data['pw1'];
        }

        if (!empty($data['email'])) {
            $this->email = $data['email'];
        }
        if (!empty($data['vid'])) {
            $this->verification_id = $data['vid'];
        }
        if (!empty($data['fname'])) {
            $this->first_name = $data['fname'];
        }
        if (!empty($data['lname'])) {
            $this->last_name = $data['lname'];
        }

        if (!empty($data['image'])) {
            $this->image_name = $data['image'];
        }
        return $this;
    }

    public function index()
    {
        try {
            $query = "SELECT * FROM `profiles` ";
            $q = $this->conn->query($query) or die('Unable to query');
            while ($row = $q->fetch(PDO::FETCH_ASSOC)) {
                $this->data[] = $row;
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        return $this->data;
        return $this;
    }

    public function show()
    {
        $query = "SELECT * FROM profiles WHERE users_id=".$this->id;
        $STH = $this->conn->prepare($query);
        $STH->execute();

        $result = $STH->fetch(PDO::FETCH_ASSOC);
        return $result;

    }

    public function signup()
    {
        try {
            $usrname = "'$this->username'";
            $qr = "SELECT * FROM users WHERE username=" . $usrname;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user = $STH->fetch(PDO::FETCH_ASSOC);

            $email = "'$this->email'";
            $qr = "SELECT * FROM users WHERE email=" . $email;
            $STH = $this->conn->prepare($qr);
            $STH->execute();
            $user2 = $STH->fetch(PDO::FETCH_ASSOC);
            if (!empty($user)) {
                $_SESSION['Message'] = "Username already exists";
                header('location:signup.php');
            } elseif (!empty($user2)) {
                $_SESSION['Message'] = "Already register with this email";
                header('location:signup.php');
            } else {
                $verification_code = uniqid();
                $query = "INSERT INTO users(id, unique_id, verification, username, password, email, is_active, is_admin)
    VALUES(:id, :uid, :vrfsn, :un, :pw, :email, :ia, :iad)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':uid' => uniqid(),
                    ':vrfsn' => $verification_code,
                    ':un' => $this->username,
                    ':pw' => $this->password,
                    ':email' => $this->email,
                    ':ia' => 0,
                    ':iad' => 0,
                ));
                $_SESSION['Message'] = "Successfully signup. Login for continue";
                $msg = "Click the below link for verify your email address.<br/> http://test.sumonmhd.com/Views/verify.php?vid=$verification_code";

                $msg = wordwrap($msg, 70);
                mail("$this->email", "sumonmhd.com", $msg);

                $last_id = $this->conn->lastInsertId();

                $query = "INSERT INTO profiles(id, users_id)
    VALUES(:id, :uid)";
                $stmt = $this->conn->prepare($query);
                $stmt->execute(array(
                    ':id' => null,
                    ':uid' => $last_id,
                ));
                header('location:login.php');
            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();

        }
    }

    public function login()
    {
        $usrname = "'$this->username'";
        $password = "'$this->password'";
        $qr = "select * from users where username =  $usrname && password = $password";
        $STH = $this->conn->prepare($qr);
        $STH->execute();
        $user = $STH->fetch(PDO::FETCH_ASSOC);

//        echo "<pre>";
//        print_r($user);
//        die();
        if (isset($user) && !empty($user)) {
            if ($user['is_active'] == 0) {
                $_SESSION['Message'] = "<h3>Your account not verified yet. Check your email and verify</h3>";
                header('location:login.php');
            } else {
                $_SESSION['user'] = $user;
                header('location:index.php');
            }
        } else {
            $_SESSION['Message'] = "<h3>invalid username or password</h3>";
            header('location:login.php');
        }

    }

    public function verification()
    {
        $verification_code = "'" . $this->verification_id . "'";
        $qr = "SELECT * FROM users WHERE verification = $verification_code";
        $STH = $this->conn->prepare($qr);
        $STH->execute();
        $user = $STH->fetch(PDO::FETCH_ASSOC);


        if (isset($user['verification'])) {
            $verification_code = "'" . $this->verification_id . "'";
            $query = "UPDATE users SET is_active = 1 WHERE verification =" . $verification_code;
            $STH = $this->conn->prepare($query);
            if ($STH->execute()) {
                $_SESSION['Message'] = "<h1>You're verified now. Thank you !</h1>";
                header('location:login.php');
            }
        }
    }

    public function profileupdate(){
        try {
            $query = "UPDATE profiles SET first_name = :fn, last_name = :ln, image =:img, modified = :md WHERE users_id =".$this->id;
            $stmt = $this->conn->prepare($query);
            $stmt->execute(array(
                ':fn' => $this->first_name,
                ':ln' => $this->last_name,
                ':img' => $this->image_name,
                ':md' => date('Y-m-d h:m:s'),

            ));
            $_SESSION['Message'] = "Profile Successfully updated";
            header('location:index.php');
        } catch (PDOException $e) {
            header('location:index.php');
        }
    }
}