<?php
include "../vendor/autoload.php";
use App\users\users;

$obj = new users();
$allUsers = $obj->index();
//echo "<pre>";
//print_r($allUsers);
if (isset($_SESSION['user']) && !empty($_SESSION['user'])) {

    ?>


    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8"/>
        <title>Registration form Template | PrepBootstrap</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css"/>

        <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body>

    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Brand</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="profile.php">Profile <span class="sr-only">(current)</span></a></li>
                        </li>
                    </ul>
                   
                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true"
                               aria-expanded="false">
                                <?php
                                echo ucfirst($_SESSION['user']['username']);
                                ?>
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="logout.php">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
    <div class="container">

        <table border="1" align="center">
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
            <?php
            foreach ($allUsers as $one) {


                ?>
                <tr>
                    <td><?php echo $one['users_id'] ?></td>
                    <td><?php echo $one['first_name'] ?></td>
                    <td><?php echo $one['last_name'] ?></td>
                    <td><img src="<?php echo "images/".$one['image'] ?>" width="200" height="150"></td>
                    <td>
                        <a href="show.php?id=<?php echo $one['users_id'] ?>">Show</a>
                        <a href="profile.php?id=<?php echo $one['users_id'] ?>">Edit</a>
                        <a href="delete.php?id=<?php echo $one['users_id'] ?>">Delete</a>
                    </td>

                </tr>
            <?php } ?>
        </table>

    </div>
    </body>
    </html>
<?php } else {
    $_SESSION['Message'] = "Login for continue";
    header('location:login.php');
} ?>
