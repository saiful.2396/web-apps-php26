<?php
include"../vendor/autoload.php";
use App\users\users;
$obj = new users();
//echo "<pre>";
//print_r($alldata);
//die();

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST['uname'])) {
        if (strlen($_POST['uname']) > 5 && strlen($_POST['uname']) < 10) {
            if (!empty($_POST['pw1'])) {
                if ($_POST['pw1'] == $_POST['pw2']) {
                    if (strlen($_POST['pw1']) > 4 && strlen($_POST['pw1']) < 10) {
                        if (!empty($_POST['email'])) {
                            $obj->prepare($_POST)->signup();
                        } else {
                            $_SESSION['Message'] = "Email Required";
                            header('location:signup.php');
                        }
                    } else {
                        $_SESSION['Message'] = "password should be 5-10";
                        header('location:signup.php');
                    }
                } else {
                    $_SESSION['Message'] = "Password does not match";
                    header('location:signup.php');
                }
            } else {
                $_SESSION['Message'] = "Password can't be empty";
                header('location:signup.php');
            }
        } else {
            $_SESSION['Message'] = "User name must be 6 - 10";
            header('location:signup.php');
        }
    } else {
        $_SESSION['Message'] = "user name can't be empty";
        header('location:signup.php');
    }
} else {
    $_SESSION['Message'] = "Opps something going wrong!";
    header('location:signup.php');
}