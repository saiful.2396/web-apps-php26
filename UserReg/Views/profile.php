<?Php
include "../vendor/autoload.php";
use App\users\users;

$obj = new users();
$mydata = '';
if (isset($_GET['id'])) {
    $mydata = $obj->prepare($_GET)->show();
} else {
    $_GET['id'] = $_SESSION['user']['id'];
    $mydata = $obj->prepare($_GET)->show();
}

?>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Registration form Template | PrepBootstrap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css"/>

    <script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</head>


<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Brand</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="profile.php">Profile <span class="sr-only">(current)</span></a></li>
                    </li>
                </ul>
                
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">
                            <?php
                            echo ucfirst($_SESSION['user']['username']);
                            ?>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="logout.php">Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>
<form role="form" method="post" action="profile_update.php" enctype="multipart/form-data">
    <div class="col-lg-6">

        <div class="form-group">
            <label for="InputName">First Name</label>

            <div class="input-group">
                <input type="text" name="fname" class="form-control" name="InputName" id="InputName"
                       placeholder="Not set yet" value="<?php
                if (!empty($mydata['first_name'])) {
                    echo $mydata['first_name'];
                }
                ?>">
                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
            </div>
        </div>

        <div class="form-group">
            <label for="InputName">Last Name</label>

            <div class="input-group">
                <input type="text" name="lname" class="form-control" name="InputName" id="InputName"
                       placeholder="Abraham" value="<?php
                if (!empty($mydata['last_name'])) {
                    echo $mydata['last_name'];
                }
                ?>">
                <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
            </div>
        </div>
        <img src="<?php echo "images/".$mydata['image'] ?>">
        <input type="file" name="image"/>
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>"/>
        <input type="submit" name="submit" id="submit" value="Update Profile" class="btn btn-info pull-right">
    </div>
</form>
<h1>
    <?php
    if (!empty($_SESSION['Message'])) {
        echo $_SESSION['Message'];
        unset($_SESSION['Message']);
    }

    ?>
</h1>

</body>
